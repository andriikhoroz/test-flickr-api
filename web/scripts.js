var app = angular.module('flickrApp', ["ngRoute"]).config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/list",
            controller: "listCtrl"
        })
        .when("/single/:farm/:server/:id/:secret/:size", {
            templateUrl: "/single",
            controller: "singleCtrl"
        })
        .otherwise({
            template: "<a href='/#!/'>Home</a><h2>404</h2><p>Page Not Found</p>"
        });
});

app.controller('listCtrl', function ($scope, $http) {
    $http({
        method: "GET",
        url: "/recent"
    })
        .then(function mySucces(response) {
            var statusText = '';
            $scope.data = response.data;
            $scope.recentFlickrImages = [];

            if ($scope.data.success === 1) {
                if ($scope.data.data.photos.photo.length > 0) {
                    angular.forEach($scope.data.data.photos.photo, function (photo, key) {
                        $scope.recentFlickrImages.push(photo);
                    });
                    statusText = 'Recent Flickr Images'
                } else {
                    statusText = 'No Images'
                }
            } else {
                statusText = response.data.message;
            }

            $scope.statusMsg = statusText;
        }, function myError(response) {
            $scope.statusMsg = response.statusText;
        });
});

app.controller("singleCtrl", function ($scope, $routeParams) {
    $scope.photo = {
        'id': $routeParams.id,
        'size': $routeParams.size,
        'farm': $routeParams.farm,
        'server': $routeParams.server,
        'secret': $routeParams.secret
    };

    $scope.imageSizes = {
        'q': 'Small',
        'z': 'Medium',
        'b': 'Large',
        't': 'Thumbnail',
    };
});