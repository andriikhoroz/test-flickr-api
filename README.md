Test Flickr API
===============

Application that would take the images from the required source and
will help user to navigate between images and sizes.

**Installation**

1. composer install
1. bower install
1. php bin/console server:start

Open http://localhost:8000