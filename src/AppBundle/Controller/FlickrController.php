<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FlickrController
 *
 * @package AppBundle\Controller
 */
class FlickrController extends Controller
{
    /**
     * @Route("/recent", name="recent")
     */
    public function recentAction(Request $request)
    {
        $flickrApi = $this->get('app.flickr_api');
        $images = $flickrApi->getRecent(25);

        $response = new Response();
        $response->setContent(json_encode($images));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
