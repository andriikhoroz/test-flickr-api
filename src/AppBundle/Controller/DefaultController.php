<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/list", name="list")
     */
    public function listAction(Request $request)
    {
        return $this->render('default/list.html.twig', []);
    }

    /**
     * @Route("/single", name="single")
     */
    public function singleAction(Request $request)
    {
        return $this->render('default/single.html.twig', []);
    }

}
