<?php

namespace AppBundle\Utils;

/**
 * Class FlickrApi
 *
 * @package AppBundle
 */
class FlickrApi
{
    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * FlickrApi constructor.
     *
     * @param string $apiKey
     */
    public function __construct(string $endpoint, string $apiKey)
    {
        $this->endpoint = $endpoint;
        $this->apiKey = $apiKey;
    }

    /**
     * @param int $count
     * @return mixed
     */
    public function getRecent(int $count = 10)
    {
        $params = array(
            'api_key' => $this->apiKey,
            'method' => 'flickr.photos.getRecent',
            'format' => 'json',
            'per_page' => $count,
            'nojsoncallback' => 1
        );

        $encodedParams = array();
        foreach ($params as $k => $v) {
            $encodedParams[] = urlencode($k) . '=' . urlencode($v);
        }

        $url = $this->endpoint . '/?' . implode('&', $encodedParams);

        return $this->fetch($url);
    }

    /**
     * @param string $url
     * @return mixed
     */
    private function fetch(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Flickr PHP API class');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        //To prevent issue with SSL certificate
        //Should not be used on production
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        $response = curl_getinfo($ch);

        if ((int)$response['http_code'] == 200) {
            return ['success' => 1, 'data' => json_decode($output)];
        } else {
            return ['success' => 0, 'message' => curl_error($ch)];
        }

        curl_close($ch);
    }
}